---
layout: page
title: About
permalink: /about/
---

This is a blog about occultism, witchcraft, and chaos magick.

You can call me Acrux. I am performing some occult practices and experimenting new things in my own magick journey, which has become more conscious for the past few years. In 2019, I started to write notes on my books, then I bought a diary and the notes became longer texts. At some point, it looked like a good idea to share my current reflections with more people. So here we are.
The internet is such a beautiful place, isn't it?

To get in touch, write to: 

talk to acrux *at* gmail *dot* com.


### Why *Liber Noodles*?

There is no reason for picking this name. Or any reason that my present self is aware of. After a couple of weeks creating many potential titles, *Liber Noodles* suddenly popped in my mind and it just felt right. 
