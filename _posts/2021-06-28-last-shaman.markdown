---
layout: post
title:  "A magickal look at: The Last Shaman documentary"
date:   2021-06-28 01:27:34 +0200
categories: chaos magickal-look 
featured-image: /media/lastsha1.jpg
---

I started by typing ‘magick’ in the Netflix search box. The result was a list of various titles. In the middle of horror movies, classics like The Craft, and series about illusionists, freemasonry, I saw [The Last Shaman](https://www.netflix.com/se-en/title/80121855) documentary film. In the movie preview, a skinny pale boy with a backpack walks in a typical crowded South American avenue. I know those avenues very well, even though I have never been specifically in Peru, where this 2017s documentary was shot. So, I opened my heart to check out what the pale gringo was doing in his search for a deeper meaning in the Amazon area, as the movie description explained, and watched it with a magickal look - like an experiment.

![A generic white man face close ](/media/lastsha5.jpg)


The young man is James Freeman, a wealthy boy, the son of two renowned academics in Physics and Neuroscience. We face the struggles of Freeman as an adult who suffers from depression. He is under pressure in the academic world and seems to be deeply unhappy with everyone’s expectations on him. An overachiever kid who got into the best university in the US - even though they don’t mention the name of the institution, we can imagine it is Harvard (they are in Boston). A quick internet research says he was actually a ‘preppy’, but I am not sure. 

The point here is: he felt oppressed by others' expectations for so long that he didn’t see any reason to be alive anymore. The guy forgot how to live. In his own words "I look at the sunset, I feel nothing. [..] I have no ambition. I have no desire other than to feel human again." 

He gives himself one year to feel it. If he doesn’t, he will allow himself to take his own life. He thanks US laws for making it possible to buy a shotgun in a mall.

James gets his backpack, his paleness, and boards on a trip to the Peruvian Amazon forest. To find ayahuasca.

Of course.

## Statement of intention

I don’t sympathize with the things James represents at all. A privileged rich boy gets dependent on anxiolytics during his finals yadda yadda. But there was something really authentic there. I felt his suffering. It was written all over his face and that particular type of deep loneliness is hard to ignore. James is very honest about his emotions and his desire to end everything by jumping from a bridge. He doesn’t seem to be one of those who are using their discontent as an excuse for not achieving what they were meant for. Making fun of his sense of loss would be too easy. So I chose to listen to what he was saying. I think it was something very close to what I was feeling only a few years ago, after I did the opposite as he did: I started to use some old known European magic traditions to experience a different reality. But when you look closely, you see that we actually did the same thing. The only difference is that wealthy James goes to an impoverished community to learn something new, whereas I (born in the outskirts of South America) go to one of the wealthiest places on Earth, Scandinavia, to learn something new.

"Instinctual passions are stronger than reasonable interests", says Freud in the classic [Civilization and its Discontents](https://en.wikipedia.org/wiki/Civilization_and_Its_Discontents). I like to take these *instinctual passions* as glimpses of our individual true will, in contrast to the struggles we all must share as part of civilization. James' uneasiness came from the environment he was exposed to for his whole life. He was trying to reasonably explain his condition of someone who has everything, and feels nothing, with the same logic and principles from the society that has put him in this situation.

The struggles he shares in the documentary are indeed very well known to most of us today: there is something wrong with the lifestyle we are following. All this pressure to be successful in very determined ways cannot be the final and only answer to our life purpose. When James says that his only desire is to find a reason to live, and for that he leaves everything behind (with some of his family money as a backup, of course), he goes out there *with an intention*. 

It is when the act of magick starts. 

## Means of manifestation

In the movie, we see some shots from James’ parents in Boston, inconsolable after doing everything they could for their offspring. At some point, his mother says how much sorrow she felt knowing that her son was off there, *away from civilization.* I could only think: which civilization? Her dear child was in [Iquitos](https://en.wikipedia.org/wiki/Iquitos), Peru. A city with half million people, considered the capital of the Peruvian Amazon. Just because certain cultures look strange to you, it doesn't mean there is no dignity in the way they live. How dare that woman say that about that place? After living for so long (directly or indirectly) on the exploitation of those people?

Ok, let’s move on to what matters in this exercise. Magick.

*"[...] if we trouble ourselves to think about it, the institutions of our culture have no other means of manifestation than the way they dictate our usage of time and space. To question or challenge this usage is a powerful technique for changing both culture and our experience of reality."*
([Occult experiences in the home](https://oeith.co.uk/books/), Duncan Barford)

James departs in a search for a new way to manifest something he couldn't in his previous reality. The world he inhabited was preventing him from experiencing a new way to look at life. I wonder how different his path would be if he had laid his eyes on some occult books, for example.

When we talk about practicing magick, we often need to think if there is a way to see this magick happening. Otherwise, how would the results manifest? They need to be possible. 

*"Decide what you want to occur. Ensure that what you want to occur has a means of manifestation. Choose an experience. Decide that the experience means the same thing as what you want to occur."*
([Advanced magick for beginners](https://www.goodreads.com/book/show/6800563-advanced-magick-for-beginners), Alan Chapman)

I think that James did well in looking for ways to see the change he wanted. The journey itself was his means of manifestation.

## Resistance, awareness and belief

His first contact with ayahuasca happens in a place close to the city, where they charge gringos who want to try the tea. The group is led by Guillermo, the first shaman we see on the screen. At this point, we begin to notice how ayahuasca business is changing the area and the life of those people in Iquitos. James doesn’t take the tea that night, because he is still on his medicine, and people warned him there is a potential risk of death by the interaction between the drugs. He chooses to participate in the ceremony as an observer. 

Then the unexpected happens: a man, another gringo, who had the tea, dies in the ceremony. He was next to James. The image of the actual body being closed in a morgue drawer is shown. James says “I was there when he died, I smelled what a dead body smells, and I have never seen a dead person in my life before.”

We notice the impact on him. I couldn’t help myself but think, as a Latin person myself, that the first time I saw a dead body was at the age of 9. One of my best friends died of a disease related to the sanitary conditions of our neighborhood. I could relate to adult James as the way I felt as a child facing a deceased so close. Yes, I remember the smell. Many people from my childhood kept dying along in my teenage years; grief became commonplace, but never stopped to be painful. 

I wanted to laugh about that adult man seeing a sample of the raw world right here, in front of him. But I couldn’t. I know how impressive and terrifying it is to understand how fragile life is. 

Seeing my friend's corpse at such a young age made me think that it could be myself in that casket. There is something in this experience that simply changes you. Entirely. And we can’t avoid the same thought when we think that it could be James in that morgue drawer. 

He then understands that what he is trying to do is real. And it can kill him - when he is looking for a reason to be alive.

At a second moment after this bizarre experience, James says that that man deserved better assistance. He feels that the community has failed him. This is a constant in the life of most people in disadvantageous regions; it is part of our day to day reality. The abandonment of institutions and lack of accountability in a society run by corruption is a fact. 

In the end, nobody is condemned for that man’s death. 

James leaves Guillermo’s guidance - wisely - and goes after someone else to help him with his journey. Someone more reliable or that could be closer to him. At that point, we never see James speaking Spanish, for example. And of course, he manages to find the next shaman in his journey: Don Ron Wheelock, the gringo shaman. A shaman whose native language is English. 

Surprisingly, Don is very aware of the ayahuasca business exploitation and even says he feels guilty for being a foreigner making use of the learnings he got from the locals. He is one of the few shamans who doesn't charge for his services, but a mentee must live with him and do what he says. Oldschool, fair enough. I think he was a good mentor, to be honest. He introduces the young man properly to the odds and conflicting realities of the land. He contextualizes and translates many local nuances. New understandings help our pale American. 

At some point, Don says "You come here for enlightenment, I can help you. But you do the work". I like how he presents this novelty to James, who seemed to be only waiting for a sip of a liquid to magically fix his mind. 

James spends nearly a month working on Don's modest property, feeding the animals, cleaning, waking up early, and weaning his medication. His comments about working for the first time in life are a bit funny, but I can see a change timidly coming. I see James smiling for the first time. Only when he is completely free from the pills and integrated in that very plain way of living, Don invites him to try ayahuasca. 

"Healing is a combination of everything together," says Don. In the ceremony, James grins while watching Don whistling a song to the preparation pipe. It is obvious that he is enjoying every second of that. I could only think: yes, you will never try ayahuasca for the first time again. Enjoy.

Which is a lie, since everybody who tries ayahuasca says that every time is the first time. 

On the next day, James tells about the experience to the cameras, in his simple bed covered by a mosquito net. He feels triggered by what he saw and seems keen to stay longer with Don. But Don has other plans. He has his own journey to start, and he tells James that he can't be his mentor anymore. He will leave to try something new himself.

So, James is wandering in the streets of Iquitos again. Looking for his new mentor. Someone points the direction to a remote community, in the heart of the rainforest. James hires a guy to take him there in a motorized canoe, and crosses a large piece of the Amazon river. He has some sort of anger attack in this traverse through dark waters. He cries. He is obviously feeling lost and abandoned. He is afraid. It is hard to watch.

![A white young man looking lost in a canoe in a dark river ](/media/lastsha3.jpg)

When he gets in the new community, they welcome him very well. It is when he meets Pepe, the last shaman. I love how Pepe only speaks in Shipido, a Panoan language spoken only in Peru and Brazil, to the cameras. Later on we find out that he knows Spanish very well, but it is nice to see that he holds firm in his sacred native language when sharing his people's knowledge. James gets invited to be part of the daily life in the community. 

They prepare a bath of special herbs before he comes in. While cleansing his body, James says one of the most significant things so far in the documentary. He says that he is trying to engage in the local rites and ceremonies without judging, without rationalizing so much of what he is doing. He is actively believing in what those people are telling him. James is opening himself to a new reality, entirely. And then he says that *maybe this is the healing*.

*“This is the attitude of the Magical sector; beliefs are altered to suit circumstances."*
([SSOTBME](https://www.goodreads.com/book/show/1802918.S_S_O_T_B_M_E_Revised?from_search=true&from_srp=true&qid=5CyUvTpzBs&rank=1), Ramsay Duke)

*"Contemporary magick is the discipline of using belief to investigate or construct realities."*
([Occult experiences in the home,](https://oeith.co.uk/books/) Duncan Barford)

## Gnosis and charging the intention

He is taken to drink ayahuasca with other shamans in a beautiful ceremony. They sing ayahuasca songs, the icaros. Pepe tells James that those chants are taught by the plants they consume on the prior days before the ritual. It is knowledge coming directly from nature. The plants speak through the shamans’ mouths.

As soon as he starts his new guidance with Pepe, James is informed that he needs to stay four months in complete isolation, under the *dieta*. Dieta is a diet of rice, fish and Shipibo blends of tobacco. So, James keeps a camera with him in the simple wooden hut and records some reactions during this period of 120 days in the forest. Smoking that strong tobacco blend every day, many times a day, is his first challenge.

![A white young man looking dazed in a wooden cabin](/media/lastsha1.jpg)

"The moment I vomited, it started to pour rain". This is a quote from his records in the first weeks. I take it as one of his first truly shamanic experiences. He meditates, smokes, and watches nature. He later reports being scratched by one of the spirits of ayahuasca while sleeping. It is important to say that, for most nations in the Amazon, ayahuasca is an entity, or even many entities. It is part of the local cosmogony. James says he is visited by demons, spirits, etc. Lucid dreams happen. It is interesting to see that he mentions the name of some spirits (correctly), like they announced themselves when they came. He dreams that people he loves die. "I have no attachment to my previous way of life". He goes on a frenetic trance, changes to a more calm way of talking, then comes back to a feverish state, it is a roller coaster of emotions. He talks with ayahuasca. But strangely, I noticed that his body features, his face, and the way he moves were also gradually changing. It is one of the most curious things to follow in the movie.

I see this part as a clear preparation before an initiation. It is an ordeal and it is also a great inner adventure. He is discovering things about himself, cleaning his body, his mind. It is known that, in order to charge your magick intent, you can use altered states of consciousness to imbue the world with the manifestation of your will. Like the launch of your intentions into the universe. Gnosis is the magickal trance we use for that. And I see James reporting way too many trance experiences in the camera to be convinced that he was experiencing gnosis. 

If we take into consideration that James' statement of intention was so great and abstract as *to find a reason to live*, it makes sense that his effort needed to be so long as 120 days of continuous trials. This period looked very much like the Temptation of Christ - Satan visiting Jesus in the desert for 40 days and 40 nights after his baptism. 



## Initiation and rebirth

James' ordeal ends. Then, under the sunlight dancing through the leaves, some people open a hole in the ground. And he is buried.

With only his nose out (covered by a thin net that protects his whole face), the camera shows the rainforest life going on, while James is in the ground. Silence, birds singing. Life just goes on as always. Earth, plants' roots, the world. We can't see the guy. 

Seven hours pass. The shamans come back and start to dig gently with their hands. They grab James by his shoulders. The soil gives birth to a man. He is bathed with clean water. It is so emotional. His appearance is so good. He is not pale anymore, neither is tan. He looks healthier, meatier. He walks differently.

In the next cut, James seats in front of Pepe. He talks in Spanish. His Spanish is SO GOOD. He tells Pepe about his renovated wills, what he wishes, new expectations. James sings to Pepe. He sings how much he loves the shaman and life. His voice is wonderful. It seems that he has gotten what he came for.

## Belief-shift 

James stays in the local village. It is not clear how long, but he gladly joins the community for a while. He plays soccer with the children. And he compares the demanding schedule he grew up with as a privileged boy in Boston, and all the pressure to perform and achieve since he was a boy, to the simple routine the local kids have. You see the impacts that not only his spiritual journey made on him, but the whole environment and cultural shift he experienced while in Peru. That is when the arrival of a NGO shakes the peaceful environment.

The NGO is building a structure in the village, their aim is to create some sort of school of shamans. The first shaman, Guillermo, is with them. It is easy to understand that Pepe doesn’t like that, but the whole situation is imposed. James doesn’t understand why and it is not clear what is really going on. But after a couple of days, Pepe is gone without saying goodbye. They tell little about it to James, who is clearly affected by that sudden disappearance. The camera follows Pepe off, as he gets in a boat, a bus, and ends up in the big city. He was accused of conducting private rituals for tourists. My speculation here is that he was banned by the new area agreements with the NGO and probably other institutions which are involved in the ayahuasca business, obviously. They comment briefly about Pepe receiving gringos to undertake the magic tea without charging them, and that it was against the NGO rules. It is a clear reference to what he was doing with James, even though nobody says a single word about it.

The final confrontation with the larger reality that engulfs the small community where James found his peace is the peak of the narrative here.

They used James as a frame to portrait the final destination of Pepe.

The last shaman.

![A indigenous man in shaman white clothes smiling ](/media/lastsha4.jpg)

## Conclusions

In the end, there are some final testimonials from James’ family, they comment on how great their relationship became after all, when James came back after his one year in Peru, etc. And then there is James’ voice over.

![A white man looking goog with his backpack and a crowded city as background ](/media/lastsha2.jpeg)

He says that we don't need to go through what he went through to achieve that awakening. Ayahuasca itself told him we don’t need it to find enjoyment and reasons to be alive. This is why I wrote about my magickal look at it - everything started with a simple exercise (I opened Netflix looking for something that would be magickally meaningful) and it presented me with many magick concepts along the narrative. 

The movie was produced and directed by the Israeli actor Raz Degas. He is credited for three films as a director on [IMDB](https://www.imdb.com/name/nm0214641/). One of these is Manifesting Greatness. I will just paste the description of the short movie here:

*“Do you believe in everyday magic? Synchronicities, déjà vus, intuition? Raz Degan, director, actor and producer, tells his story how Jägermeister affected his life and helped him to manifest his dreams. This is his journey.”*

*.