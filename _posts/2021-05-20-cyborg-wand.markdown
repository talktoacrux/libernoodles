---
layout: post
title:  "The cyborg and the magickal wand"
date:   2021-05-15 01:27:34 +0200
categories: reflection chaos
featured-image: /media/book-cyborg.jpg
---

*"Though both are bound in the spiral dance, I would rather be a cyborg than a goddess"*, the ending statement of Donna Haraway in the famous essay The Cyborg Manifesto, a work that lands in the middle of a Star Wars fever, pre-fall of the Berlin Wall, and the launch of the Hubble telescope. Since I started to search for some sort of comfort in general magical thinking, it is more common to see magick in mundane readings. 
![Cyborg Manifesto cover](/media/book-cyborg.jpg){: style="float: left"} 

The image of the cyborg lingers within me since I read the manifesto again, for the second time - ten years after the first one. Today, it is impossible to read it without thinking about the work of Silvia Federici (*The Caliban and the Witch*) and her reflections about the mechanization of the proletarian class. The desires factory that deviates our true will is intrinsic to the logic presented in both texts, particularly when I look at them as an occult practitioner, and not as an academic student. Because we cannot conceive the practice of magick today without rethinking our relationship with the concept of private property, labour power, and owning our bodies/time.

When I do magick, I feel more authentic; for there is absolutely no reason to do it - only the simple will to do so. To exercise our right to do magick, we don't need anything to start. Only our minds and will. Magick is available to everyone, at any age, with any type of body, from anywhere. And the most important thing: although it is nice to do it with other people, it is not a requirement to be accompanied (far from it, I'd say). Imposing systematic organization into this process is normal, since every other thing in our life gains complexity while we develop it - or when we, as humankind, consent in the social contract. So, of course magic as we know it in the western world had gained coding and systematization along the years. But its basic principles remain accessible, no big interfaces required.

When I got in contact with paganism for the first time, I was presented with that traditional wiccan styled magic. Many things were meant to be developed with mere instinct, and some others required very specific tools and situations to play out. 
![Equal Rites cover](/media/equal-rites.jpg){: style="float: right; max-width: 350px"}
Later, I found books that present some highly elaborated ways to contact the divine and to exert our intentions with magic. Then things started to split into only a few options. I like to take Terry Pratchett's Discworld concepts about witches and wizards to draw the differences between these differing approaches I faced. 
The first one, the witch, is the mother goddess, the earth, the nature, the spiral dancer, the healer; the woman in the kitchen cooking the next ointment or love potion. She is also our Granny Weatherwax, character of the book *Equal Rites*, an illiterate woman who lives in a wooden hut and knows everything about potions and animals from the forest (she can even get into their minds, to see what they see around). The second one is the archimage, the scientific lord of the cabbala banishings, the man in the robe, the master of triangles. He is our Archchancellor Cutangle, another character from the same book, a professor of the highest degrees at the Unseen University and Archmagi of the Silver Star. The witch is dreamy and sensitive, her magic work moves towards good intentions for her community; the wizard is pragmatic and methodical, his magic work serves as a ladder to a higher self, titles, self-discipline. 

Fitting into the witch figure had some appeal, but I considered myself a "person of science", so the wizard image was more seducing in my idealization of a magical life. Luckily, I was lazy enough to just curl up on the sofa for a long time, reading the books and observing. I tried a few simple things from both ways, but nothing seemed to resonate with what I wanted. And the truth is that, in fact, I didn't know what I wanted. I only knew what I didn't want: to be judged by others, and to depend on some demanding system as a means to do something greater with my life. In the beginning, I renounced the witch because they looked delusional, fragile, and too dependent on symbols that represent everything I don't want to perform. Like they were an ideal token made for the male gaze (I don't think this anymore, but that was how I saw things at that time). Or the portrait of a femininity I refused to perpetuate. Taking the image of the man in the robe, the ceremonial leader, on the other hand, was an attempt to be cool, 'one of the boys'. Like embodying an idealization of equality (or belonging) that simply doesn't exist. And I nearly fell into this trap.

Unlike the binarism of that question, the figure of the cyborg in Donna Haraway essay doesn't respond to any preconceived path. They don't need the approval of the mother or the father; neither need to be saved. There is no aim to be a goddess or a vessel of powerful dark beasts. While our life as part of the proletarian class depends on the established family role model, like the private property itself, the cyborg is free to follow an individualistic path and also to attach themselves to infinite formats of affection. 

![A 3000 year-old vignette from the Djedkhonsuiefankh funerary papyrus on display in the Cairo Egyptian Museum.](/media/nuit.jpg){: style="width: 100%"}

At some point, I almost believed that I could embrace a manifestation of Babalon. Even while feeling unsatisfied with her role in this great play. Her crimson dance could be a way to be free. But it didn’t seem to be quite the answer for how I find myself right here, right now, in the 21st century. So many sources of our suffering come from the patriarchy, capitalism, post-colonialist politics. It is evident that the way to truly overcoming it is not performing the old dualism of whore/mother, it would be too simplistic. Nor is becoming one of the guys. 

<p class="right"><em>"The Manifestation of Nuit is at an end".</em></p>

<p class="right">(Liber Al vel Legis, Alesteir Crowley). </p>

I see it as an opportunity to shake the same old paradigms and rebuild an entirely different thing. We can either be the witch and the wizard at the same time, or nothing at all. I don't know if it will lead us to a real change in the end, but what is the enlightenment if not a continuous awakening? When I understand that doing magick is my way to be accountable for my existence, that is what I am looking for. A way to be free from the platforms which were imposed on me as the only options, things that were determined before I was even born. This is not renouncing completely to the inherited knowledge and traditions we feed on, but paving a new yellow brick road. There is space to rethink how to awake; to take the chance to reshape the foundations of our beliefs. 

I want to build something else. 

Let's see what I might find in the way I am setting forward. 

