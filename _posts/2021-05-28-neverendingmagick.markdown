---
layout: post
title:  "First impressions on occult books"
date:   2021-05-28 01:27:34 +0100
categories: readings chaos
featured-image: /media/visualmagick.jpg
---
In the past few days, I spent long hours reading some 'essential' books about chaos magick. I had read a few of them in the past - or at least had tried to read, leaving some of them in the middle, because I couldn't take the content seriously. Now I see that I was just not prepared. I had to live some key experiences in order to grasp what they were really talking about. I imagine how hard it can be for someone without any study group as a support (or any previous exposure to mysticism) to start reading this basic recommended literature for the first time. It is a jungle of symbols and concepts that even the easiest prose can look like an ethereal world presented by a lunatic. Luckily, interacting with other magicians, being introduced to basic practices by some reasonable people, and then trying out simple things during this process made my beginning easier. Perhaps, more conscious and lesser sacred, lesser mysterious. Still, there were **two books** that stuck with me in those crucial first months when I decided to take a decisive step into my spiritual journey. They are the ones that I could comprehend at that point, and they made a big difference in how the occultism world started to firm foundations in my life.

![Visual Magick, Brazilian cover](/media/visualmagick.jpg){: style="margin: 0 20px; float: left"} The first one was [Visual Magick, by Jan Fries](https://www.goodreads.com/book/show/471224.Visual_Magick). When I put my hands on this book, I already knew what was a sigil and how people used it. But it was only when I read the first chapter  that I learnt the principle of its mechanics. Or what was operating behind all the actions of shuffling letters and drawing signs, the reasons why it *could be* important to forget it, and so on.



<p class="right"><em>"Identity, the aspect of self of which you are conscious, is a creature with many desires and many fears. Ego is an agglomeration of habits and beliefs, many of which are in conflict with each other. If you desire a thing, you have to recognize that this desire is not shared by all aspects of your identity, and that many of your personalities tend to disagree".</em></p>

<p class="right"><small>(Excerpt from Visual Magick) </small></p>



 It is a very very practice-oriented book, with some short insights and contextualization before the actual lessons and examples. And regardless of its hands-on nature, it leaves the path open enough, so the readers can exercise their own creativity - from sigil making to spirit-guardian invocations.



The second one to help me in my first steps was [The Spiral Dance, by Starhawk](https://www.goodreads.com/book/show/73869.The_Spiral_Dance?from_search=true&from_srp=true&qid=2Z4VWvsBRU&rank=1). It is not a book about chaos magick, but it exposed me to an author who was willing to revisit her previous statements with all the changes she saw in the world and in her own magic circles along the years since the book’s first publication. For that, I recommend the 1999 edition by [HarperOne](https://www.harpercollins.com/products/spiral-dance-the-20th-anniversary-starhawk-4?variant=32217649348642) or something more recent, if such a thing even exists. The best parts are the two introductions; the first for the 20th anniversary, and the second for the 10th one. The book has commentaries on all the reflections and rituals, where Starhwak points out why she wrote those things when she was 28, compared to what she thinks at the age of 48, when the last footnotes were added. It is an invaluable reading experience. 

![The Spiral dance cover](/media/spiral-dance.jpg){: style="margin: 0 20px; float: right"} 

Getting inspired by people who are not afraid of facing their past or changing their opinion is important. If not crucial. 



<p class="right"><em>“The mysteries of the absolute can never be explained - only felt or intuited. Symbols and ritual acts are used to trigger altered states of awareness, in which insights that go beyond words are revealed".</em></p>





<p class="right"><small>(Excerpt from The Spiral Dance) </small></p>



Reading it was essential to understand that first I needed to go out there and try things, so coming back to other books would present new layers of interpretation. Today I know that I kept this early insight until today, in the back of my mind. It led me to give a second chance to other readings. And it worked.





