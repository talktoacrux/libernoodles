---
layout: post
title:  "Book review: Magia, by Alan Chapman"
date:   2021-06-06 08:27:34 +0100
categories: book-review 
featured-image: /media/visualmagick.jpg
---

What comes after awakening?

[*Magia*](https://barbarouswords.com/book) is one of those books that you should just let take you by the hand. Trying to rationalize it from the start will only bring you pain. And if you are expecting an upgrade in the author's famous [*Advanced Magick for Beginners*](https://www.goodreads.com/book/show/6800563-advanced-magick-for-beginners), hold your horses. Magia is something completely new.


![Magia cover](/media/magia.jpg){: style="margin-right: 20px; float: left"}

"It is appropriate to begin

teaching with silence

because that is where

we begin."

These are the first words we find in the text. They belong to the first of twelve teachings, The Mystery of Magickal Reality. And they set the tone that will follow until the end, in The Serpent. From the first to the last word, the content is presented in verses, in a very particular layout that helps the reader to experience the topics along the reading shape (form and content very well used here). It feels like it should be read out loud. It also holds some sort of poetic tone mixed with guided meditation. Still, Magia is not a poem. But it was, indeed, born from a retreat run by Alan in Pelion, Greece, in May of 2019. It is a transcription of the lessons he gave during this period, intercalated with some questions people asked him during the process - his students star in the verses as cute insects, like bees, caterpillars, and butterflies. The discussions resemble texts of classic Greek plays. The aesthetic worked well and broke the profound spiritual, almost sacred, rhythm in the rest of the book.

The aim is to guide the reader across an internal journey of self-molding, pattern recognition, awakening, and some deep things I had only flirted with briefly in my magickal practices. However, they are things I had already visited on my psychoanalysts' couch. The intention here is to bring up new tools to unlock deeper understandings in the places where other beliefs prevent us from crossing certain limits. Thus, my first question *'What comes after the awakening?'.* It is very hard to translate the main idea behind the whole thing without sounding a little bit lightheaded. But the text is about this.

Before launching the book, Alan had published the retreat videos in a website that now is redirected to his new[ Barbarous Words](https://barbarouswords.com), where anyone has free access to the original audios. I had followed it last year until the Casting Shadows (the second lesson). I enjoyed the journey and recommended it to a few friends. But I didn't go until the end myself - mostly because of other magick work I was prioritizing back then. But the most important thing I did (unconsciously) before digging into the book last week was binge listening to[ Duncan Bardorf's podcast](https://oeith.co.uk/podcast/). I got a taste for listening to calming Duncan while I walk in the forest nearby. Chapman cultivates a similar tone, it is a delight. (Do they invoke the same entity to achieve it?). I know that they are[ BFFs](https://www.urbandictionary.com/define.php?term=BFF). Duncan is even credited in Magia's note of thanks. So, it doesn't sound like a big surprise or a crazy synchronicity that a very specific episode of his podcast served as a preparatory conversation to me. I refer to[ The Terrors of Awakening](https://anchor.fm/oeith/episodes/OEITH-003-The-Terrors-of-Awakening-e1023mh). In this episode, Duncan made me revisit my own experiences of a constructed reality that is entangled *from and to* myself, a happening that can be also interpreted as the ultimate legit religious experience, as he explains serenely. I believe that the first twenty minutes of that audio is a proper reflection, a lesson in itself, that can help us engage in Magia from the beginning. 

The afterword was a surprise. I won't get into details to not ruin people's experiences, but I might come back to it in the future. I'd like to highlight only this part:

"And  though it may burn,

the forest will not be destroyed.

We look to the west

to find you [..]"


In the summer of 2019 I went to Greece for the first time in my life. No, I wasn't in the Pelion retreat - I haven't even been exposed to any of Chapman's work at that time. Funny thing or not, I was myself looking for some way to experience the truth. I spent hours going from temple to temple, meditating at the Mediterranean sea, swimming in that beautiful transparent salty waters. I was reading about[ HGA](https://en.wikipedia.org/wiki/Holy_Guardian_Angel) and trying many new things for the first time. But my strongest memory from those days was following the news about the Amazon forest fire. I remember talking about it with people on the bus, in the suburbs of Athens. The image of the fire was so strong in my mind, that the tones of sand collapsing from ancient gods ruins in the peaks were frequently obfuscated by the burning rainforest in my phone screen.

This memory crossed me like an arrow when I read The Magia Prayer, the last thing I found in the book. And it is not meant to me to understand it right here, right now. I will let it sink for a while before raising more reflections.

It is a book to be digested longer than the reading experience itself. Listening to the audios in the website is part of the process, I'd say. It is easier to perform the meditations with them. 

Its is also a book for people who are pursuing something more than what the usual magick practice can take us. 

"*What is preventing me from waking up?*" the caterpillar asks. "*What is the final step?*"