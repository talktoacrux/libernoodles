---
layout: post
title:  "Cultural appropriation in magick"
date:   2021-07-31 06:01:33 +0200
categories: reflection magick 
featured-image: /media/lastsha1.jpg
---

![the nocturnal house at Koala Gardens, Kuranda, Australia, where the lighting for these geckos is very dim and red](/media/lizard.jpeg)


This is old news. But the debate is far from ending.

I have seen way too many white folks talking about cultural appropriation in witchcraft and only a few comrades from marginalized communities discussing it from a magickal perspective. There are some very young people speaking out on Youtube, but very often their critical point converts into the final conclusion "just be respectful". But what does *being respectful* mean?

About ten years ago, a friend dressed like a fortune-teller 'gipsy' woman to go to a costume party. The word *gipsy* is considered a direct translation to the Portuguese *cigana*, [which is not considered offensive](http://jornal.usp.br/especial/revista-usp-117-a-construcao-das-identidades-ciganas-no-brasil/) by the [Romani people](https://en.wikipedia.org/wiki/Romani_people) in South America (at that time we lived in São Paulo, Brazil, where people speak Portuguese). I created a deck of cards for her, people would joke and call it 'the carnival tarot' later. Each card had its own symbol: a bottle of beer, money, representations of sexual positions, a joint, some traditional local dishes, etc. Things that I thought would make people have a good laugh. My friend opened the deck facing down to anyone at the party, and pretended she was a fortune-teller, reading their near future in the card they would pick, making a different voice tone, and creating a singular personality for her character. We had fun.

Today we would never do it again. It is common understanding that it is a form of cultural appropriation. And you see, we were a group of latin people using another cultural token as a costume. It didn't matter that in our heads we respected Roma history and traditions, that was a very poor way to celebrate it. In the end, we were using the stereotypes we inherited from the traditional Western way of looking at that particular nation to have fun. There is nothing fun about it; it perpetuates the idea that Romani women are reduced to that simple stereotype. A very harmful one, considering that many real Roma (fortune tellers or not) are persecuted every day by police officers in the big cities (from Brazil to Sweden).

*"Even after ten years in the field of Roma representation and rights, I sometimes wonder: How is it possible to get away with stereotyping the Roma people in such a disparaging way? [..] Although more professional and academic Roma are emerging, many remain stuck in a cycle of poverty at the margins of society; some are refugees, fleeing discrimination and persecution; others cope with stigma and belonging."*

― Cristiana Grigore, [on Newsweek](https://www.newsweek.com/halloween-costume-roma-gypsy-tropes-1469002).


![A bunch of playing cards messily spread, facing down](/media/cards.jpeg)

I usually come back to this story in my past, because it is a very simple example about how our actions might not look bad or do any direct damage to someone and still be harmful in the long run.


### **What is cultural appropriation?**

Far from wincing at feathered headdresses on white heads in Coachella, or problematizing blond dreadlocks, the real problem lives more in institutionalized appropriation than in individual ones. Take for an example: when a famous fashion brand features [typical Chola](https://www.latinorebels.com/2016/12/01/chola-thats-who-i-am/) styled earrings on the runway, and then sells them for thousands of euros or dollars. How much of this money goes to the decades of bias against latinas who proudly wore those rings in their ears while bearing two or three shifts of low pay jobs for years to serve the families or industries of those who will purchase the latest fashion trend goodies? For years those rings were a sign of resistance and a cultural icon of affirmation - quite often used to identify those women in the crowd.

Today it is very common to see other cultures’ elements (anything that is considered different, iconic, or exotic) being fetishized as consumable goods, normalized to the eyes of the public, deprived of its meaning, flattened as one more stupid replicated cheap accessory by fast fashion stores.

*“Cultural appropriation is the adoption or theft of icons, rituals, aesthetic standards, and behavior from one culture or subculture by another. It generally is applied when the subject culture is a minority culture or somehow subordinate in social, political, economic, or military status to the appropriating culture.”*

-- [American Indian Health & Family Services organization](https://www.aihfs.org/pdf/8-1-16%20Cultural%20Appropriation.pdf)

Nobody will die, of course. But what means for that particular marginalized group watching one more thing being stripped of their lives to serve the urge of others for novelty? Taking away their rights, dignity, and dreams wasn’t enough? Now they take their symbols too. It might look small, but think about it for a minute or two.

Some months ago the [band Gojira released a new single](https://www.gojira-music.com/amazonia), in an album largely inspired by indigenous culture from the Amazon forest area. At first, I frowned on the image of a French band making money using native Amazonian music instruments. Then I found out that they sent all the gained money to non-profit local organizations. Like, everything (not only part of it). It seems a bit better. But is it still problematic? I don’t know, honestly. I just wish those communities didn’t depend on others’ goodwill to strive for existing with the least dignity.

Today it is easy to find totems of cultural appropriation and well-intended efforts to bring different cultures close together everywhere. The frontier between the harm and the gain is a fine line. And there is no definitive answer. There is no law about it.

I like to always look at where the profit is, if there is some. It brings clarity to the matter.

Capitalism is sneaky, hard to get, thus it appropriates everything.

### **Cultural appropriation in witchcraft?**

Condemning the crime of burning sage is trendy in cultural appropriation discussion right now. I don't doubt people's good intentions in pointing out the issue to their friends. It is known that original North American nations were forbidden to burn sage until 1978, and recent increasing demand of white sage by the market is making things harder for the natives to acquire it - and proceed with their regular spiritual practices. The problem is that we naively believe that is enough just informing our friends that smudging is cultural appropriation.

![A wage stick burning upwards in a brown pot](/media/smudge.jpeg)

There is no effective change in this type of thing without public policies to regulate the market or long term education in the magic circles (this one is especially hard because there is no such thing as a regulation of the occultist community). But the questions that floats over our heads right now probably are more or less like: and if I grew sage in my backyard? Is it still a bad form of cultural appropriation when I am not exploiting natives' lands or making profit from my personal sage production? Is it disrespectful if I am smudging in the privacy of my home, as an individual silent practice, without publicizing it?

That is where an invisible border comes in to hunt us all.

## Magickal appropriation

### **Contextualization and a pinch of history**

I grew up watching my mother burn palo santo sticks in our house, always walking counterclockwise in each room (for: banishing), once a week or so. On Sunday, we would go to the catholic church, and she wouldn't have the communion wafer because she wasn't "doing the sacrament of penance very often". After the church, we would meet one of my aunts and go to a spiritist centre, where people acted as *mediuns* to give voice to dead people. Also, at that time, there was a small wooden figure in our front garden who we greeted every day. We called that one Exu.

Exu is an orixá (ourisha) with many aspects, depending on the Afro-Brazilian religious current we talk about, who is in charge of the crossroads. I like to say that Exu is a non-binary entity, so I refer to Exu as *they/them* - even though there are women, men and children exus in some currents (and yes, Exu can be the Exu or an exu. It is confusing, I know). They are the mischief, very clever and resourceful. Mom gave them booze on Mondays. When our friends from candomblé came to visit, they told us that exus guard the worshipping shrines, not peoples' homes. But we kept our Exu there anyway. When visited by the Christian friends, they would also greet Exu (before making the sign of the cross).

That is how pagan life mixes with everything else you can ever imagine in that corner of the world.

None of us were formally initiated in candomblé or umbanda, the two main Afro-Brazilian religions. However, my father-side grandmother was. And some of my mother-side cousins would later be, when adults. The issue in understanding these religions is that: they are oral traditions. And this changes everything. Their teachings survived and adapted along [500 years of involuntary diaspora](https://en.wikipedia.org/wiki/African_diaspora) process. Yes, I am talking about Angola, Nigeria, Moçambique and other countries whose people were taken by Dutch, Portuguese and Spanish slave traders to do forced labour overseas.

![Some old terrestrial globe balls in a dark light](/media/maps.jpeg)

Those African people were enslaved. And their religious practices were immediately forbidden: even their given names were replaced by typical Christian ones. Fair to mention that Catholicism was also compulsory, of course. I'd like to highlight here that those African nations were roughly mixed when they got there, when they had conflicts among them and many didn't share the same beliefs. They also got in contact with the local indigenous, with their own creeds, that were already striving to survive a genocide in the paradise.

Chaos opened the road to merge everything available, developing the major Afro-Brazilian religions we have now, and their several currents (I dare to say that a similar thing also happened to our fellow people from Caribbean regions up to Mexico, with santeria, curanderismo, vodou and all possible kinds of brujeria). It was shaped by what we know as [syncretism](https://en.wikipedia.org/wiki/Syncretism). Like when people learnt to disguise orixás inside saints statues, and proceed with what they remembered from their spiritual traditions.

Not to be confused with that antiquity syncretism from Hellenic times, Greco-Egyptian style. Or the later Roman appropriation of Greek gods. In this case, we can see it as the dominant culture using appropriation for expanding purposes. This time, in recent colonialism, we have a story of syncretism forged by misery and death; used as a tool by the helpless and oppressed ones.

It is hard to organize the spiritual knowledge they developed along the way in that part of the world. Differently from other religions or religious movements, there are no basic written registers of some South American traditions; even the ancient local spirituality from the natives are hard to unroll, for many reasons. Not because the registers were lost, but because they were never produced. And they still remain oral traditions today.

Important to say that the few things remaining are the outcome of centuries of endurance by all the exploited ones who remain alive to tell the story today.

The Exu in the garden had to cross centuries of strenuous change and adaptation to be there, watching over us.

But since none of our home inhabitants was a strict practitioner of any Afro-Brazilian religion, was that cultural appropriation? Was my mother's prayers with Holy Rosary less worthy because she was meddling with spiritism,and worshipping Exu?

I would say no to both questions.  But there is a difference between them.

And there lives an invisible border.

**Is everybody living happily ever after?**

*"Persecution of these* *[Afro-Brazilian religions](https://www.google.com/url?q=https://rlp.hds.harvard.edu/faq/african-derived-religions-brazil&sa=D&source=editors&ust=1627736269097000&usg=AOvVaw0xXLDyoWeoH_I5ugGipGON)**, whose adherents are largely poor black Brazilians,* *[has been around since at least the 19th century](https://www.google.com/url?q=http://www.publicadireito.com.br/artigos/?cod%3D13d83d3841ae1b92&sa=D&source=editors&ust=1627736269097000&usg=AOvVaw1LYRd5p8l9sXhklzereFQ5)**.*

*But the current wave of religious bigotry is more personal, and more violent, than in the past. As the Washington Post recently* *[reported](https://www.google.com/url?q=https://www.washingtonpost.com/world/the_americas/soldiers-of-jesus-armed-neo-pentecostals-torment-brazils-religious-minorities/2019/12/08/fd74de6e-fff0-11e9-8501-2a7123a38c58_story.html?utm_campaign%3Dacts_of_faith%26utm_medium%3DEmail%26utm_source%3DNewsletter%26wpisrc%3Dnl_faith%26wpmm%3D1&sa=D&source=editors&ust=1627736269097000&usg=AOvVaw3H4i1qFeloejxNV8LuTnwo)**, Afro-Brazilian priests are being harassed and murdered for their faith. Candomblé and Umbanda practitioners fear leaving their homes. Terreiros have closed due to death threats."*

([source](https://theconversation.com/evangelical-gangs-in-rio-de-janeiro-wage-holy-war-on-afro-brazilian-faiths-128679#:~:text=According%20to%20Brazil%27s%20Commission%20to,an%20increase%20on%20previous%20years.&text=As%20the%20Washington%20Post%20recently,and%20murdered%20for%20their%20faith))

Today I wouldn't worship an orixá without getting permission from some high priestess of their cult. Does it mean I judge my friends who do it recklessly? No, never.

It means that I respect my cousins who are now in the priesthood of umbanda, like: I recognize the path our land ancestors took to make this available to us. I will not take it for granted because of my own magick pathwork, inherited from other lands and cultures.

In contrast, as a chaos magician, I have no problem in adopting Viking, Greek, Egyptian, or Mesopotamian paradigms, for example. For the simple fact that they are dead religions. I also have no problem in using Christianity, since I know it very well as it was the imposed belief in my land during colonial times.

Sometimes I feel that I appropriated chaos magick. It allows me to navigate the rough sea of this feeling of belonging to everything and nothing.

You see, cultural appropriation in witchcraft is personal. We have little or no control over it, because the practice of magick belongs to each one of us in infinite forms. Being respectful is a grey area, and it frequently depends on where you are and with whom you are.

![Pastel color paints in a surfice](/media/colors.jpeg)

Respect for others, respect for yourself, respect for history.

How can our actions towards it improve our spiritual experience? Or make something good for unprivileged people's lives? There is no final answer. But asking questions is important.

### **Is chaos** **magick** **a form of cultural appropriation?**

Essentially, no. But it can be.


We can start by looking at its foundations. Like if we were someone who is completely new to the concept. Which books would we find to start off? In a quick internet search, most recommended authors are: Peter Carroll, Ray Sherwin, Austin Osman Spare, Phil Hine, etc. What do all they have in common? Well, everything. Men, white, UK. My point here is not criticizing these people by who they are. Not to mention that I actually like their work; I will always come back to what they developed in their first contributions to chaos magick. It would be just stupid to refuse these teachings.

Needless to say, it is in the early 20th century, when Spare experiments with other beliefs and dogmas, that he ignites the flame of chaos. One of his main influences is Madame Blavatsky, who traveled the world and is largely recognized by starting the Western Esotericism - drawing concepts from hinduism, buddhism, etc. That's to say we can't expect to live in a bubble, apart from being influenced by others or by experiences we are exposed to. Or disconnected to the rest of the planet. But we can be more *conscious* about what we take from it all, or what we do with it.

It is not a coincidence that we still borrow so much from Aleister Crowley's work, for example. A Spare's contemporary, being a ‘man of his time’. I think their contemporaneity and the persons they became define some sort of zeitgeist of that period. Come on, both men's works project what later became the foundations of chaos magick, as I understand it.

What is the British empire if not the biggest symbol of exploitation of other lands until only some decades ago? And what does it mean for chaos magick seminal works and influences? I believe those men were in the best position to start it, with their privileges and birthrights. I don't want to flatter them, but I can be grateful for the outcome of their experiments without overlooking the fact that they drank from other cultural sources to achieve it. The absurdity of the British empire allowed them to access things, connect the dots, and do the travels they needed to. Unfortunately, African descendants in South America at that time couldn't do the same. Indigenous people were too busy trying to survive while the rest of the world was collapsing in wars and plague. This is what I want to point out when I think that chaos magick is not necessarily cultural appropriation now; but its basic concept could only be conceived with it. Of course there was no such concept in that time: it doesn’t mean we can’t look at the past with this fresh critical look. Nor does it mean that we must refuse the knowledge because now we know it was built with cultural appropriation.

That is why this text became so long. We can’t talk about it frivolously or take it personally.

![A cristal globe with colorful light inside in a dark room](/media/globe.jpeg)

Moving forward to more recent years, the most read authors remain sharing the same cultural background. But I have the feeling that the practices are changing. Being deeply rooted into the British cultural movements of the 70s is a characteristic of chaos culture, the community started there as we know it now. Does it mean the current authors are bad? No, far from it. Can we move on from this, now that chaos seems to be spreading all over the world? I think so. It might take a long time, but I hope to be alive some day to see chaos magick by the lens of people with other backgrounds - and what they have to add to our leaps of faith.

### **But what can we do about magickal appropriation, after all?**

Well, in fact, nothing. But I can think about four things that would be beneficial to anyone concerned about the topic. These are just suggestions, not my attempts to make rules. ;)

1) Take magick (or magic, sourcery, as you wish) to the ones who can make good use of it. And who are open to discuss magick without avoiding the hard topics or making little of them.

2) Talk to people who live in the paradigm you are working with. Listen to them. Read what people from that cultural background are writing. 

3) When in doubt about adopting a new paradigm because of the lack of material or trustworthy sources, consider giving it up. You can always use an old and very known one instead, for example.

4) Look for magicians that don't look like you. This is not only regarding magickal practices. But also about gender, sexuality, nationality, everything you can think about.

☆.

<br>

*“Witchcraft is the recourse of the dispossessed,*

*the powerless, the hungry and the abused.[..]*

*It turns on a civilization that knows the*

*price of everything and the value of nothing.”*

― Peter Grey, [Apocalyptic Witchcraft](https://www.google.com/url?q=https://www.goodreads.com/work/quotes/26516150&sa=D&source=editors&ust=1627736269102000&usg=AOvVaw3MnnJjvn14hfbTG-TQNG-7)


<br>

***


<br>

#### **Recommended readings**

Magic, Witchcraft and Curanderismo: Let's talk about cultural appropriation

[https://aldianews.com/articles/culture/social/magic-witchcraft-and-curanderismo-lets-talk-about-cultural-appropriation](https://www.google.com/url?q=https://aldianews.com/articles/culture/social/magic-witchcraft-and-curanderismo-lets-talk-about-cultural-appropriation&sa=D&source=editors&ust=1627736269103000&usg=AOvVaw0a2FUFiPSt5rl1PjXzqwbK)

Is chaos magick cultural imperialism?

[http://web.archive.org/web/20051124063537/http://www.key23.net/occulture/post/258](https://www.google.com/url?q=http://web.archive.org/web/20051124063537/http://www.key23.net/occulture/post/258&sa=D&source=editors&ust=1627736269104000&usg=AOvVaw2VVdF3SoBHjMUbjMYwzyVN)

The ethics of using sage for energy cleansing
[https://foreverconscious.com/the-ethics-of-using-sage-for-energy-cleansing](https://www.google.com/url?q=https://foreverconscious.com/the-ethics-of-using-sage-for-energy-cleansing&sa=D&source=editors&ust=1627736269104000&usg=AOvVaw14eNFiLa9mlXTDqKw_qMkl)

Chaos magick as cultural imperialism: some thoughts

[http://streamsofconsciousness.org/commonplace/chaos-magick-as-cultural-imperialism-some-thoughts/](https://www.google.com/url?q=http://streamsofconsciousness.org/commonplace/chaos-magick-as-cultural-imperialism-some-thoughts/&sa=D&source=editors&ust=1627736269104000&usg=AOvVaw0yzJ5QZF0Wm2r-JHDbdIEY)

How to be a witch without stealing other people's cultures

[https://mashable.com/article/witchtok-problematic-witch-cultural-appropriation](https://www.google.com/url?q=https://mashable.com/article/witchtok-problematic-witch-cultural-appropriation&sa=D&source=editors&ust=1627736269105000&usg=AOvVaw1pjtf-Xt9A0b1UIUcYsR7x)

Chola style – the latest cultural appropriation fashion crime?

[https://www.theguardian.com/fashion/2014/aug/15/-sp-chola-style-cultural-appropriation-fashion-crime](https://www.google.com/url?q=https://www.theguardian.com/fashion/2014/aug/15/-sp-chola-style-cultural-appropriation-fashion-crime&sa=D&source=editors&ust=1627736269105000&usg=AOvVaw1MNoYPVLqTNEMiBBbuxDPj)

